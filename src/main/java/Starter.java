


public class Starter {

    public static void main(String[] args) {
     Warrior Knight = new Warrior.WarriorBuilder("Knight", 100).setEnergy(200).setSuperPowerEnabled(true).setWeapon(5).build();
     Warrior Princess = new Warrior.WarriorBuilder("Princess", 50).setEnergy(30).build();
     Warrior Viking = new Warrior.WarriorBuilder("Viking", 90).setEnergy(200).setWeapon(2).build();

        System.out.println(Knight.toString());
        System.out.println(Princess.toString());
        System.out.println(Viking.toString());




    }
}
