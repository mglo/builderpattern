import java.security.KeyStore;

public class Warrior {

    //required fields
    private String name;
    private int health;

    //optional parameters
    private boolean isSuperPowerEnabled;
    private int weapon;
    private int energy;

    public Warrior(WarriorBuilder warriorBuilder) {
        this.name = warriorBuilder.name;
        this.health = warriorBuilder.health;
        this.energy = warriorBuilder.energy;
        this.isSuperPowerEnabled = warriorBuilder.isSuperPowerEnabled;
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    public int getEnergy() {
        return energy;
    }

    public boolean isSuperPowerEnabled() {
        return isSuperPowerEnabled;
    }

    public int getWeapon() {
        return weapon;
    }

    @Override
    public String toString() {
        return "Warrior{" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", isSuperPowerEnabled=" + isSuperPowerEnabled +
                ", weapon=" + weapon +
                ", energy=" + energy +
                '}';
    }

    public static class WarriorBuilder {

        //required fields
        protected String name;
        protected int health;

        //optional parameters
        private boolean isSuperPowerEnabled;
        private int weapon;
        private int energy;

        public WarriorBuilder(String name, int health) {
            this.name = name;
            this.health = health;
        }

        public WarriorBuilder setSuperPowerEnabled(boolean superPowerEnabled) {
            this.isSuperPowerEnabled = superPowerEnabled;
            return this;
        }

        public WarriorBuilder setWeapon(int weapon){
            this.weapon = weapon;
            return  this;
        }

        public WarriorBuilder setEnergy(int energy){
            this.energy = energy;
            return this;
        }

        public Warrior build(){
            return new Warrior(this);
        }
    }


}
